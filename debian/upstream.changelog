Levee, or
Captain Video!

Description

   Levee is my attempt to write a vi clone. It was originally written for
   [1]USCD Pascal I.5 on the [2]Terak 8510a, ported to USCD Pascal 4.0 on
   the [3]Sage and Pinnacle 68000-based microcomputers, ported back to
   I.5, but rewritten in P2 (a Pascal derivative language I wrote in 1983
   or thereabouts), rewritten in Pascal for an MP/M box, then my friend
   John Tainter and I re-re-rewrote it in C so we could run it on our
   newly-purchased [4]Atari 520STs in 1985, and then I re-re-re-rewrote it
   for FlexOS when I was working at CPT in (yuck!) Eden Prairie, MN.

   When Atari began to roll belly-up, we both abandoned that platform for
   PCs and re-re-re-re-etc-wrote it for MS-DOS, and when I started using
   [5]Linux in 1992 I rewrote it AGAIN, and after a few years where I
   didn’t use it because of the existance of nvi (and Caldera releasing vi
   as open source) I started using it and maintaining it again.

Source Code

     * [6]4.0 is, after approximately 30 years, a new major release, with
       a few bug fixes, a few feature enhancements, and a pretty major
       reworking to make the code more portable.
       bug fixes:
         1. fix a 39 year old bug where changing the last line in a file
            wouldn’t refresh properly.
         2. fix a newer bug where if a file wasn’t newline terminated a
            random garbage byte would be added to it when written out
         3. (not a bug, but annoying) I’d accepted a patch that mapped ‘g’
            to ‘1G’, which has the annoying side-effect of making the
            previously-unused key right next to `h' into a fumblefinger
            that was regularly making me leap away from what I was working
            on, so I backed this right on out.
       feature enhancements:
         1. Properly implement the ‘#’ & ‘%’ expansions in ex-mode file
            lists.
         2. Implement “ as an ex-mode command (comment, mainly for ctags
            variants that use ” to separate out extended positioning
            data.)
         3. Implement the :tag (ex-mode) & ^] (visual mode) commands, plus
            an tagstack that can be backed down.
       In addition, I went on a pretty extensive code shuffling spree to
       move all of the OS-dependent code out of the main source and into a
       collection of OS-dependency modules, and then turned around and
       reported Levee to Windows 10 (via the Win32 interface) and OS/2.
     * [7]3.5b adds compatability with the creeping bloatware that is C on
       OSX 10.10 – apparently getline() and getcontext() have suddenly
       become part of the thrice-damned standard library, so my getline()
       and getcontext(), which have been compiling without complaint for
       over 20 years now, suddenly conflict with the clown car of chaos
       that is the standard of the week.
       This wasn’t something I discovered; Peter Aronoff tried to compile
       levee on a 10.10 machine and had the C compile throw a tantrum on
       him (the copy of levee I use was compiled on a 10.5 machine and has
       been working without complaint since then), so he patched the bug,
       added a new feature (g maps to 1G for symmetry with G taking you to
       the end of the file), and did some editing on the documentation.
       And then I went in and updated my email address (I am never going
       to move back to Chicago, so staying in the chi.il.us
       pseudo-top-level domain is kind of silly) and it’s now version
       3.5b.
       Enjoy the minimal changes and wait for OSX 10.11 to make more of my
       functional space magically reserved.
     * [8]3.5a had to be released because I was bitten by the double curse
       of Xcode becoming more gnu out from under me and modern commercial
       Linuxes becoming, if possible, even more gnu than they were before.
       On top of these configuration changes, I’ve also added another new
       feature in the form of “0” as a movement command like you’ll find
       in the other, higher priced, versions of vi.
     * [9]3.5 has, after a gap of about 15 years, a new feature in the
       form of the “!” command in visual mode. Other than that, there are
       no changes from 3.4p.
     * [10]3.4p has been reworked so that it can be configured with
       [11]configure.sh, and has the additional minor feature that it
       better supports consoles and gui windows that come in different
       sizes. This is the first new-feature-release in quite some time (I
       believe the last feature went into levee sometime prior to 1998).
       This release is thanks to Felipe Augusto van de Wiel, from the
       [12]Debian Linux project, who sent me mail on 15-Jun-2007 asking if
       I’d be willing to accept some patches and roll levee up to a new
       release so that Debian wouldn’t be carrying around a decade’s worth
       of differences from the baseline. I’ve not gotten his patches yet
       (version 3.4q will no doubt follow 3.4p in quick succession,) but
       after he sent me the mail I got the whim to build levee on a
       freeBSD box. That didn’t work (and it didn’t work in a fairly
       spectacular fashion,) so I spent 5 hours replacing the old “here
       are a dozen or so settings you have to set by hand” arrangement
       with one that [13]doesn’t involve getting a Masters degree in Levee
       coding. It’s uncertain whether configure.sh will get along with
       dos, the atari.st, rmx, or flexos (signs say “no”), but you should
       be able to run configure.sh on a Unix machine, then export the
       configured code to your dos/atari/rmx/flexos box and build it
       there.
     * [14]3.4o has no features that haven’t been in levee since the late
       1980s, but it compiles cleanly with cc -Wall (for values of cc
       which translate to gcc 2.7.2 – I don’t officially support any newer
       versions of that thrice-damned compiler – or, maybe, pcc,) which is
       important, I guess.

     And, just in case you’re wondering, no, I never watched the
     [15]Captain Video TV show when I was young – I don’t even think I
     was even aware of the name except as a silly name until just
     recently – I just liked the funny juxtiposition of names.

     * [16]home
     * [17]sitemap
     * [18]colophon
     * [19]/~orc/Code/levee/index.text
       Tue Jun 16 23:14:57 PDT 2015

References

   1. http://en.wikipedia.org/wiki/UCSD_Pascal
   2. http://en.wikipedia.org/wiki/Terak_8510/a
   3. https://en.wikipedia.org/wiki/SAGE_Computer_Technology
   4. http://en.wikipedia.org/wiki/Atari_ST
   5. http://www.kernel.org/
   6. http://www.pell.portland.or.us/~orc/Code/levee/levee-4.0.tar.bz2
   7. http://www.pell.portland.or.us/~orc/Code/levee/levee-3.5b.tar.bz2
   8. http://www.pell.portland.or.us/~orc/Code/levee/levee-3.5a.tar.gz
   9. http://www.pell.portland.or.us/~orc/Code/levee/levee-3.5.tar.gz
  10. http://www.pell.portland.or.us/~orc/Code/levee/levee-3.4p.tar.gz
  11. http://www.pell.portland.or.us/~orc/Code/configure
  12. http://www.debian.org/
  13. http://www.pell.portland.or.us/~orc/Code/configure
  14. http://www.pell.portland.or.us/~orc/Code/levee/levee-3.4o.tar.gz
  15. https://en.wikipedia.org/wiki/Captain_Video_and_His_Video_Rangers
  16. http://www.pell.portland.or.us/~orc/
  17. http://www.pell.portland.or.us/~orc/sitemap.html
  18. http://www.pell.portland.or.us/~orc/ingredients.html
  19. http://www.pell.portland.or.us/~orc/Code/levee/index.text
